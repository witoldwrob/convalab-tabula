FROM jruby:9.2-jdk

RUN apt-get update -qq && apt-get install -y build-essential \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

WORKDIR /app


RUN groupadd convalab
RUN useradd -ms /bin/bash tabula -g convalab
RUN chown -hR tabula:convalab  /app
USER tabula:convalab

RUN echo 'gem: --no-rdoc --no-ri' >> ./.gemrc

ENV GEM_HOME /usr/local/bundle
ENV PATH $GEM_HOME/bin:$PATH
RUN gem install bundler -v '< 2' \
  && bundle config --global path "$GEM_HOME" \
  && bundle config --global bin "$GEM_HOME/bin"

# don't create ".bundle" in all our apps
ENV BUNDLE_APP_CONFIG $GEM_HOME

EXPOSE 9292
CMD ["jruby", "-G", "-r", "jbundler", "-S", "rackup", "-o", "0.0.0.0", "config.ru"]

# these didn't work as ONBUILD, strangely. Idk why. -JBM
COPY --chown=tabula:convalab Gemfile Gemfile.lock Jarfile Jarfile.lock ./
RUN bundle install && jruby -S jbundle install
COPY --chown=tabula:convalab . .
